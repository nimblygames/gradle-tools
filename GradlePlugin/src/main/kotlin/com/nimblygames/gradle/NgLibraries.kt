/*
 * Copyright 2020 Nimbly Games, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("KDocMissingDocumentation", "unused")

package com.nimblygames.gradle

// kotlin
const val kotlinVersion = "1.3.71"
const val kotlinSerializationVersion = "0.20.0"
const val kotlinCoroutineVersion = "1.3.5"

// ktor
const val ktorVersion = "1.2.6"

// test
const val junitVersion = "4.12"

// crypto
const val bouncyCastleVersion = "1.58"

// json
const val jsonSimpleVersion = "20170516"

// xml
const val jdomVersion = "2.0.2"

// logging
const val slf4jVersion = "1.7.30"
const val log4jVersion = "2.13.1"
const val kotlinLoggingVersion = "1.7.9"

// logging
const val slf4jApiNotation = "org.slf4j:slf4j-api:$slf4jVersion"
const val log4jSlf4jImplNotation = "org.apache.logging.log4j:log4j-slf4j-impl:$log4jVersion"
const val log4jCoreNotation = "org.apache.logging.log4j:log4j-core:$log4jVersion"
const val kotlinLoggingNotation = "io.github.microutils:kotlin-logging:$kotlinLoggingVersion"

// jni bindings
const val libgdxVersion = "1.9.10"
const val lwjglVersion = "3.2.1"

// common libs
const val guavaVersion = "28.1-jre"
const val commonsTextVersion = "1.1"
const val commonsLangVersion = "3.6"
const val commonsCompressVersion = "1.20"
const val openCsvVersion = "4.1"
const val pircbotxVersion = "2.1" // java irc library
const val restfbVersion = "1.46.0"
const val nettyAllVersion = "4.0.52.Final" // async io library
const val pdfBoxVersion = "2.0.8"

const val jsoupVersion = "1.8.3"

// aws
const val awsSdkVersion = "1.11.649"

// javax mail
const val javaxMailVersion = "1.6.0"

// ant
const val antVersion = "1.10.1"

// html utils
const val yuiCompressorVersion = "2.4.8"
const val rhinoVersion = "1.6R7"

// servlet container
const val tomcatVersion = "8.5.23"

// ssh
const val jschVersion = "0.1.55"

// http TODO needs updating
const val httpClientVersion = "3.1"
const val seleniumServerStandaloneVersion = "2.53.0"

// cassandra
const val cassandraAllVersion = "3.0.15"
const val cassandraDriverCoreVersion = "3.3.0"

// lzma compression
const val xzVersion = "1.8"

// proguard
const val proguardVersion = "6.2.2"

// greenmail email testing framework
const val greenMailVersion = "1.5.11"
