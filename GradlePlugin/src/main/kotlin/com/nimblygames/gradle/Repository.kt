/*
 * Copyright 2020 Nimbly Games, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nimblygames.gradle

import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.RepositoryHandler

/**
 * Adds publishing repositories from the gradle.property files that start with maven.repository.url.$repositoryIndex and
 * where the URL contains nimblygames.com.
 */
fun RepositoryHandler.addNimblyGamesPublishRepositories(project: Project, isSnapshot: Boolean) {
   addPublishRepositories(project, isSnapshot, true)
}

/**
 * Adds publishing repositories from the gradle.property files that start with maven.repository.url.$repositoryIndex.
 *
 * @param isOnlyPublishToNimblyGames only add publish repositories that include "nimblygames.com"
 */
fun RepositoryHandler.addPublishRepositories(project: Project, isSnapshot: Boolean, isOnlyPublishToNimblyGames: Boolean = false) {
   for (repositoryIndex in 0..10) {
      // @formatter:off
        if (project.hasPropertyOrEnvVar("maven.repository.url.$repositoryIndex")
            && (!isOnlyPublishToNimblyGames
                || project.getPropertyOrEnvVar("maven.repository.url.$repositoryIndex").contains("nimblygames.com"))
            && ((project.getPropertyOrEnvVar("maven.repository.ispublishsnapshot.$repositoryIndex").toBoolean() && isSnapshot)
                || (project.getPropertyOrEnvVar("maven.repository.ispublishrelease.$repositoryIndex").toBoolean() && !isSnapshot))) {
            // @formatter:on
           project.logger.info("Adding publishing repository url ${project.getPropertyOrEnvVar("maven.repository.url.$repositoryIndex")}")
           addRepositoryPropertyIndex(project, repositoryIndex)
        }
   }
}


/**
 * Adds the repositories defined in the gradle.properties files to the [RepositoryHandler].
 */
fun RepositoryHandler.addGradlePropertyDownloadRepositories(project: Project) {
   for (repositoryIndex in 0..10) {
      // @formatter:off
        if (project.hasPropertyOrEnvVar("maven.repository.url.$repositoryIndex")
                && project.getPropertyOrEnvVar("maven.repository.isdownload.$repositoryIndex").toBoolean()) {
            // @formatter:on
           addRepositoryPropertyIndex(project, repositoryIndex)
        }
   }
}

private fun RepositoryHandler.addRepositoryPropertyIndex(project: Project, repositoryIndex: Int) {
   maven { artifactRepository ->
      artifactRepository.name = "maven.repository.url.$repositoryIndex"
      artifactRepository.url = project.uri(project.getPropertyOrEnvVar("maven.repository.url.$repositoryIndex"))
      artifactRepository.credentials { passwordCredentials ->
         passwordCredentials.username = project.getPropertyOrEnvVar("maven.repository.username.$repositoryIndex")
         passwordCredentials.password = project.getPropertyOrEnvVar("maven.repository.password.$repositoryIndex")
      }
   }
}
