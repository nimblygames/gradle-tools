/*
 * Copyright 2020 Nimbly Games, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nimblygames.gradle

import org.gradle.api.Project

/**
 * Searches Gradle properties for [variableName], then [System.getProperty] and finally [System.getenv], returning true for the first non null occurrence.
 *
 * @return true if the Gradle, System, or Environment property named [variableName] is found, otherwise false.
 */
fun Project.hasPropertyOrEnvVar(variableName: String): Boolean {
   return findPropertyOrEnvVar(variableName) != null
}

/**
 * Searches Gradle properties for [variableName], then [System.getProperty] and finally [System.getenv], returning the first occurrence.
 *
 * @return the Gradle, System, or Environment property named [variableName] if found, otherwise throws [IllegalArgumentException].
 * @throws IllegalArgumentException if the property or environment variable isn't found
 */
fun Project.getPropertyOrEnvVar(variableName: String): String {
   return findPropertyOrEnvVar(variableName)
          ?: throw IllegalArgumentException("Couldn't find Gradle or System property or Environment variable named ${variableName}")
}

/**
 * Searches Gradle properties for [variableName], then [System.getProperty] and finally [System.getenv], returning the first non null occurrence.
 *
 * @return the Gradle, System, or Environment property named [variableName] if found, otherwise null.
 */
fun Project.findPropertyOrEnvVar(variableName: String): String? {
   return when {
      hasProperty(variableName) -> property(variableName) as String
      System.getProperty(variableName) != null -> System.getProperty(variableName) as String
      else -> System.getenv(variableName)
   }
}
