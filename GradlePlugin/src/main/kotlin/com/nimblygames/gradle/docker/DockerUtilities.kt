/*
 * Copyright 2020 Nimbly Games, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nimblygames.gradle.docker

import com.github.dockerjava.api.DockerClient
import com.github.dockerjava.core.DefaultDockerClientConfig
import com.github.dockerjava.core.DockerClientImpl
import com.github.dockerjava.zerodep.ZerodepDockerHttpClient
import com.nimblygames.gradle.docker.DockerConnectionHost.DEV01
import com.nimblygames.gradle.docker.DockerConnectionHost.LOCALHOST
import org.gradle.internal.os.OperatingSystem
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.security.KeyStore
import java.security.KeyStoreException
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory


/**
 * Searches `passFile` for `startingString` and returns the line of the file after `startingString`.
 *
 * @param passFilePath the password file to search
 * @param startingString the string to search for
 * @return the line in `passFile` after `startingString`
 * @throws IOException if an IO error occurs
 */
private fun loadLineFromFile(passFilePath: Path, startingString: String): String? {
   var foundLine: String? = null
   Files.newInputStream(passFilePath).use { fileStream ->
      BufferedReader(InputStreamReader(fileStream)).use { br ->
         var readLine = br.readLine()
         var foundStart = false
         while (readLine != null) {
            if (foundStart) {
               foundLine = readLine
               break
            }
            if (readLine.startsWith(startingString)) {
               foundStart = true
            }
            readLine = br.readLine()
         }
      }
   }
   return foundLine
}

private fun loadKeyStore(
      keyStoreType: String?, keyStorePath: String, keyStorePassphrase: CharArray?
): KeyStore {
   val keyStoreFile: File = File(keyStorePath).absoluteFile
   val keyStore = KeyStore.getInstance(keyStoreType)
   if (keyStoreFile.exists()) {
      BufferedInputStream(FileInputStream(keyStoreFile)).use { bis -> keyStore.load(bis, keyStorePassphrase) }
   } else {
      throw KeyStoreException("Keystore file $keyStoreFile does not exist")
   }
   return keyStore
}

@Suppress("SameParameterValue") //
private fun getSSLContext(
      sslType: String?,
      keyStoreType: String?,
      keyStorePath: String,
      keyStorePassphrase: CharArray?,
      alias: String?,
      privateKeyPassphrase: CharArray?,
      trustStoreType: String?,
      trustStorePath: String,
      trustStorePassphrase: CharArray?
): SSLContext? {
   val keyStore: KeyStore = loadKeyStore(keyStoreType, keyStorePath, keyStorePassphrase)
   val aliases = keyStore.aliases()
   println("Aliases of the keystore are:")
   while (aliases.hasMoreElements()) {
      val loopAlias = aliases.nextElement()
      println("alias=$loopAlias")
      val certificate = keyStore.getCertificate(loopAlias)
      println("certificate via alias $loopAlias=$certificate")
      val key = keyStore.getKey(loopAlias, privateKeyPassphrase)
      println("key via alias $loopAlias=$key")
   }
   val trustStore: KeyStore? = loadKeyStore(trustStoreType, trustStorePath, trustStorePassphrase)
   return getSSLContext(keyStore, sslType, alias, privateKeyPassphrase, trustStore)
}

private fun getSSLContext(
      keyStore: KeyStore?, sslType: String?, alias: String?, privateKeyPassphrase: CharArray?, trustStore: KeyStore?
): SSLContext? {
   val aliases = (keyStore ?: return null).aliases()
   while (aliases.hasMoreElements()) {
      val currentAlias = aliases.nextElement()
      if (!currentAlias.equals(alias, ignoreCase = true)) {
         keyStore.deleteEntry(currentAlias)
      }
   }
   val keyManagerFac = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm())
   keyManagerFac.init(keyStore, privateKeyPassphrase)
   val trustManagerFac = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
   trustManagerFac.init(trustStore)
   val sslContext = SSLContext.getInstance(sslType)
   sslContext.init(keyManagerFac.keyManagers, trustManagerFac.trustManagers, null)
   return sslContext
}

/**
 * Creates the client configuration for connecting to the developer Docker host running on 10.10.10.21.
 */
@Suppress("unused") // It's used as a library
fun createDockerClient(connectionHost: DockerConnectionHost = LOCALHOST): DockerClient {
   val builder = when (connectionHost) {
      DEV01 -> {
         val nimblyGamesDirectory = Paths.get(System.getenv("NG_DIRECTORY") ?: throw IllegalStateException("Couldn't find environment variable NG_DIRECTORY"))
         val keyStorePath = nimblyGamesDirectory.resolve("crypto").resolve("karl-desktop-docker-client.keystore").toAbsolutePath().toString()
         val passFile =
               Paths.get(System.getenv("NG_VOLUME_N_MOUNT") ?: throw IllegalStateException("Couldn't find environment variable NG_VOLUME_N_MOUNT"))
                  .resolve("NimblyGamesPassword.txt")
         val keyStorePassphrase = loadLineFromFile(passFile, "Nimbly Games Karl desktop Docker client key store passphrase")!!.toCharArray()

         val trustStorePath = nimblyGamesDirectory.resolve("crypto").resolve("nimbly_games_ca01_truststore.keystore")
         val trustStorePassphrase = loadLineFromFile(passFile, "Nimbly Games CA01 TrustStore passphrase")!!.toCharArray()
         val sslContext = getSSLContext("TLSv1.2", "JKS", keyStorePath, keyStorePassphrase, "karldesktopdockerclient",
                     keyStorePassphrase,
                     "JKS",
                     trustStorePath.toString(),
                     trustStorePassphrase)

         DefaultDockerClientConfig.createDefaultConfigBuilder()
            .withDockerHost("tcp://10.10.10.21:2375")
            .withCustomSslConfig { sslContext }
            .withDockerTlsVerify(true)
      }
      LOCALHOST -> {
         when {
            OperatingSystem.current().isLinux -> {
               DefaultDockerClientConfig.createDefaultConfigBuilder().withDockerHost("unix:///var/run/docker.sock")
            }
            OperatingSystem.current().isWindows -> {
               DefaultDockerClientConfig.createDefaultConfigBuilder().withDockerHost("npipe:////./pipe/docker_engine")
            }
            else -> throw IllegalStateException("Unsupported operating system for connecting to Docker on the localhost. Only Linux and Windows are implemented.")
         }
      }
   }
   val dockerConfig = builder.build()!!
   val dockerHttpClient = ZerodepDockerHttpClient.Builder().dockerHost(dockerConfig.dockerHost).build()
   return DockerClientImpl.getInstance(dockerConfig, dockerHttpClient)
}

/**
 * Known hosts that a Docker client configuration can be created for
 */
enum class DockerConnectionHost {
   /**
    * Nimbly Games Dev01 host
    */
   DEV01,

   /**
    * Use Docker on the localhost unix socket or Windows named pipe
    */
   LOCALHOST
}
