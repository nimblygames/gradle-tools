/*
 * Copyright 2020 Nimbly Games, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import org.gradle.api.JavaVersion.VERSION_1_8
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
   kotlin("jvm") version "1.3.71"
   id("org.jetbrains.dokka") version "1.4.0-rc"
   `maven-publish`
   signing
}

group = rootProject.group
version = rootProject.version

repositories {
   jcenter()
}

dependencies {
   implementation(kotlin("stdlib"))
   implementation(gradleApi())
   api("com.github.docker-java:docker-java-core:3.2.5")
   implementation("com.github.docker-java:docker-java-transport-zerodep:3.2.5")
}

/**
 * Is the version a snapshot or release?
 */
val isSnapshot: Boolean = project.version.toString().contains("SNAPSHOT")

publishing {
   publications {
      register<MavenPublication>("GradlePlugin") {
         from(components["java"])
         artifactId = "gradle-plugin"

         pom {
            name.set("GradlePlugin")
            description.set("Gradle plugin for providing utility functions for Nimbly Games projects")
            url.set("https://nimblygames.com/")
            licenses {
               license {
                  name.set("The Apache License, Version 2.0")
                  url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
               }
            }
            developers {
               developer {
                  id.set("KarlSabo")
                  name.set("Karl Sabo")
                  email.set("karl@nimblygames.com")
               }
            }
            scm {
               connection.set("scm:git:https://gitlab.com/nimblygames/gradle_tools")
               developerConnection.set("scm:git:https://gitlab.com/nimblygames/gradle_tools")
               url.set("https://gitlab.com/nimblygames/gradle_tools")
            }
         }
      }
   }

   repositories {
      for (repositoryIndex in 0..10) {
         // @formatter:off
         if (project.hasProperty("maven.repository.url.$repositoryIndex")
             && ((project.findProperty("maven.repository.ispublishsnapshot.$repositoryIndex").toString().toBoolean() && isSnapshot)
                 || (project.findProperty("maven.repository.ispublishrelease.$repositoryIndex").toString().toBoolean() && !isSnapshot))) {
            // @formatter:on
            maven {
               name = "maven.repository.url.$repositoryIndex"
               url = project.uri(project.findProperty("maven.repository.url.$repositoryIndex") as String)
               credentials {
                  username = project.findProperty("maven.repository.username.$repositoryIndex") as String
                  password = project.findProperty("maven.repository.password.$repositoryIndex") as String
               }
            }
         }
      }
   }
}

signing.useGpgCmd()

if (isSnapshot) {
   logger.info("Skipping signing")
} else {
   publishing.publications.configureEach {
      logger.info("Should sign publication ${this.name}")
      signing.sign(this)
   }
}

java {
   @Suppress("UnstableApiUsage") withJavadocJar()
   @Suppress("UnstableApiUsage") withSourcesJar()
   sourceCompatibility = VERSION_1_8
   targetCompatibility = VERSION_1_8
}

tasks.named<Jar>("javadocJar") {
   dependsOn("dokkaJavadoc")

   from(File(buildDir, "dokka/javadoc"))
}

tasks.withType(KotlinCompile::class.java).configureEach {
   kotlinOptions.jvmTarget = VERSION_1_8.toString()
}
