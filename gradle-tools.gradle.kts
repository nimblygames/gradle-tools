/*
 * Copyright 2020 Nimbly Games, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.gradle.api.tasks.wrapper.Wrapper.DistributionType

plugins {
   base
   id("org.jetbrains.changelog") version "0.4.0"
}

version = "1.2.0-SNAPSHOT"
group = "com.nimblygames.gradle"

tasks.getByName<Wrapper>("wrapper") {
   distributionType = DistributionType.ALL
   gradleVersion = "6.5.1"
}
