/*
 * Copyright (c) 2020 Nimbly Games, LLC all rights reserved
 */

rootProject.name = "gradle_tools"
rootProject.buildFileName = "gradle-tools.gradle.kts"

include("GradlePlugin")
project(":GradlePlugin").buildFileName = "gradlePlugin.gradle.kts"
