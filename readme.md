# Information
This project has functions and global constants that remove some boiler plate from writing Gradle scripts for Nimbly Games projects.

Merge requests are welcome. Please see the contributing.md file for the JetBrains code styles.
