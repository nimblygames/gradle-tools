# Changelog

## [Unreleased]
### Added
* Project extension functions for loading Gradle, System, or environment variables.
### Changed
* Repository functions to search for Maven repositories in Gradle and System properties and environment variables.
### Deprecated

### Removed

### Fixed

### Security
## [1.1.1]
### Added

### Changed

### Deprecated

### Removed

### Fixed
- Fixed a bug where publishing repositories weren't getting added

### Security
## [1.0.8]
### Added
- Function for publishing to all Maven repositories in the properties file.

### Changed

### Deprecated

### Removed

### Fixed

### Security
